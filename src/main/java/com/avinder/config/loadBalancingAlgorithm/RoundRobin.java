package com.avinder.config.loadBalancingAlgorithm;

import com.avinder.BackendServer;

import java.util.List;

public class RoundRobin extends lbAlgorithm {

    int currentAliveBackendServer;

    private RoundRobin() {
        currentAliveBackendServer = 0;

    }

    static RoundRobin roundRobinAlgorithm;

    public static RoundRobin getRoundRobinAlgorithm(List<BackendServer> backendServerList) {
        if (roundRobinAlgorithm == null)
            roundRobinAlgorithm = new RoundRobin();
        lbAlgorithm.backendServerList = backendServerList;
        return roundRobinAlgorithm;
    }

    @Override
    public BackendServer getAliveBackendServer() {
        List<BackendServer> aliveBackendServers = super.backendServerList;
        int count = aliveBackendServers.size();
        currentAliveBackendServer = (currentAliveBackendServer + 1) % count;
        return aliveBackendServers.get(currentAliveBackendServer);
    }
}
