package com.avinder.config.loadBalancingAlgorithm;

import com.avinder.BackendServer;

import java.util.List;

public abstract class lbAlgorithm {
    static List<BackendServer> backendServerList;
    public abstract BackendServer getAliveBackendServer();
}
