package com.avinder.config;

import com.avinder.BackendServer;
import com.avinder.config.loadBalancingAlgorithm.RoundRobin;
import com.avinder.config.loadBalancingAlgorithm.lbAlgorithm;

import java.util.ArrayList;
import java.util.List;

public class lbServerConfig {
    // here we may use builder pattern , if no of arguments are too large
    lbAlgorithm algorithmInUse;
    String ownHealthIP;

    @Override
    public String toString() {
        return "lbServerConfig{" +
                "algorithmInUse=" + algorithmInUse +
                ", ownHealthIP='" + ownHealthIP + '\'' +
                ", ownHealthPort='" + ownHealthPort + '\'' +
                ", backendServerList=" + backendServerList +
                '}';
    }

    String ownHealthPort;
    List<BackendServer> backendServerList;

    public lbAlgorithm getAlgorithmInUse() {
        return algorithmInUse;
    }

    public lbServerConfig(List<BackendServer> backendServerList) {
        this.backendServerList = backendServerList;
        algorithmInUse = RoundRobin.getRoundRobinAlgorithm(this.backendServerList);
        ownHealthIP = "127.0.0.1";
        ownHealthPort = "2000";
    }

    void setBackendServers(List<BackendServer> backendServerList){
        this.backendServerList = backendServerList;
    }

}


