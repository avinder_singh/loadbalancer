package com.avinder;

import com.avinder.config.lbServerConfig;
import com.avinder.config.loadBalancingAlgorithm.lbAlgorithm;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.util.ArrayList;
import java.util.List;


public class lbServer {
    lbServerConfig lbServerConfig;
    lbServer(lbServerConfig lbServerConfig){
        this.lbServerConfig = lbServerConfig;
    }
    public synchronized void  connectToBackendServer(Request request) throws IOException {
        lbAlgorithm lbAlgoInUse =  lbServerConfig.getAlgorithmInUse();
        BackendServer server = lbAlgoInUse.getAliveBackendServer();
        System.out.println("Server="+server.toString()+" : "+request.name);
        // open tcp connection , send data and close tcp connection
        //make http request over tcp connection

        URL url = new URL("http://"+server.runningIP+":"+server.runningPort+"/test");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        System.out.println("Content Received: "+content+" : "+request.name);
    }

    void handleRequest(int data, Request request) throws IOException {
        System.out.println("Got Data : "+data);
        connectToBackendServer(request);
        System.out.println("Data Sent: "+data+"\n\n\n\n");
    }
}
