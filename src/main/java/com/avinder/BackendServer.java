package com.avinder;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

public abstract class BackendServer {
    String runningIP;
    String runningPort;

    @Override
    public String toString() {
        return "BackendServer{" +
                "runningIP='" + runningIP + '\'' +
                ", runningPort='" + runningPort + '\'' +
                ", healthCheckAPI='" + healthCheckAPI + '\'' +
                '}';
    }

    String healthCheckAPI;
    abstract void healthIP(String IP);
    abstract void setRunningConfig(String runningIP , String port);

    abstract BackendServer start() throws IOException;
    abstract void stop();
}

class PaymentServer extends BackendServer{

    @Override
    void healthIP(String healthCheckAPI) {
        super.healthCheckAPI = healthCheckAPI;
    }

    @Override
    void setRunningConfig(String runningIP, String port) {
        super.runningIP = runningIP;
        super.runningPort = port;
    }

    @Override
    BackendServer start() throws IOException {
        int port = Integer.valueOf(super.runningPort);
        HttpServer server = HttpServer.create(new InetSocketAddress(port), 0);
        System.out.println("server started at " + port);
        server.createContext("/test", new TestHandler(port));
        server.setExecutor(null);
        server.start();
        return this;
    }

    @Override
    void stop() {

    }

    PaymentServer(String healthCheckAPI, String runningIP, String port){
        healthIP(healthCheckAPI);
        setRunningConfig(runningIP, port);

    }

}
class TestHandler implements HttpHandler {
    int port;

    TestHandler(int port) {
        this.port = port;
    }

    @Override
    public void handle(HttpExchange he) throws IOException {
        String response = "<h1>Server start success "+
        "if you see this message</h1>" + "<h1>Port: " + port + "</h1>";
        he.sendResponseHeaders(200, response.length());
        OutputStream os = he.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
