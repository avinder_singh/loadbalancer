package com.avinder;

import com.avinder.config.lbServerConfig;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static java.lang.System.exit;

public class Driver {
    public static void main(String[] args) throws IOException, InterruptedException {
        // create 2 backend servers
        List<BackendServer> backendServerList = new ArrayList<>();
        backendServerList.add(new PaymentServer("/health-check","127.0.0.1","3000").start());
        backendServerList.add(new PaymentServer("/health-check","127.0.0.1","3500").start());
        lbServerConfig lbServerConfig = new lbServerConfig(backendServerList);
        System.out.println("lbserverconfig: "+lbServerConfig);
        // start lbserver
        lbServer server = new lbServer(lbServerConfig);

        Thread[] requests = new Thread[30];

        for(int i=1;i<=30;i++){
            // create 30 parallel requests
            requests[i-1] = new Thread(new Request("Thread: "+i, server));
        }
        for(int i=1;i<=30;i++){
            // create 30 parallel requests
            requests[i-1].start();
        }
        for(int i=1;i<=30;i++) {
            requests[i-1].join();
        }
        System.out.println("finally reached here");
        exit(0);
    }
}

class Request implements Runnable{
    String name;
    lbServer server;
    Request(String name, lbServer server){
        this.name = name;
        this.server = server;
    }
    @Override
    public void run() {
        try {
            server.connectToBackendServer(this);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
